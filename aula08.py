# Generators

def cria_lista(x):
    lista = []
    for numero in range(0, x):
        lista.append(numero)
    return lista

def cria_generator(x):
    for numero in range(0, x):
        yield numero

# print(cria_lista(26))

gen = cria_generator(26)
# print(next(gen))
# print(next(gen))
# print(next(gen))
# print(next(gen))
# print(next(gen))
# print(next(gen))

# for x in gen:
#     print(x)

# Generator Comprehensions ==========================
def gen():
    for x in range(0, 10):
        yield x
gencomp = gen()
# print(next(gencomp))

gencomp = (x for x in range(0, 10))
# print(next(gencomp))


# List Comprehensions ================================
def dobro_lista(x):
    lista = []
    for numero in range(0, x):
        lista.append(numero * 2)
    return lista

# print(dobro_lista(15))

lista_comp = [x * 2 for x in range(0, 15)]
# print(lista_comp)


# Dictionary Comprehension ===========================
def gera_dict():
    dicionario = {}
    for chave, valor in enumerate(range(550, 560)):
        dicionario[chave] = valor
    return dicionario

dict_comp = {chave: valor for chave, valor in enumerate(range(550, 560))}

# print(gera_dict())
# print(dict_comp)

# =====================================================
# map, reduce, filter

#map
def quadrado(x):
    return x**2

quadrado2 = lambda x: x**2
lista = [1, 2, 3, 4, 5]

print(list(map(quadrado, lista)))
print(list(map(quadrado2, lista)))


#reduce
from functools import reduce
lista = [5, 10, 15, 20, 25]

def soma(x, y):
    return x + y

soma2 = lambda x, y: x + y

print(reduce(soma, lista))
print(reduce(soma2, lista))


#filter
temperaturas = [25, 30, 35, 36, 12, 21, 41, 22, 36]

sub_30 = lambda x: x < 30
print(list(filter(sub_30, temperaturas)))

