# API
# Frameworks -> Django, Flask, FastAPI
# pip install flask

from flask import Flask

app = Flask(__name__)

@app.route("/")
def saudacao():
    return "Bem vindo(a) a nossa aplicação!"

@app.route("/menu")
def menu():
    return "Menu: opções..."

# localhost -> máquina local -> 127.0.0.1

# Requests
import requests

# requests.get()
# requests.post()
# requests.delete()

api_url = "https://google.com.br/"

resposta = requests.get(api_url)
print(resposta.text)

import openai
# ChatGPT -> API -> openai

# ============================================
# Virtual Environment (venv)

# LINUX: 
# pip install virtualenv        #Instala a ferramenta pra criação de venv
# su root                       #Troca para o super usuario
# apt install python3.10-venv   #Instala uma dependencia pro venv
# exit                          #Volta pro seu usuario normal
# python3 -m venv ./app         #Cria o ambiente virtual em umas pasta "app"
# source ./app/bin/activate     #Ativa o ambiente virtual

# WINDOWS:
# pip install virtualenv        #Instala a ferramenta pra criação de venv
# python -m venv .\app          #Cria o ambiente virtual em umas pasta "app"
# app\Scripts\activate          #Ativa o ambiente virtual

# pip freeze > requirements.txt
# pip install -r requirements.txt

# .gitignore

# ==============================================
# UnitTest - Testes unitários

import unittest

class Testa_strings(unittest.TestCase):

    def compara_maiusculo(self):
        self.assertEqual("tiago".upper(), 'TIAGO')

    def testa_maiusculo(self):
        self.assertTrue("TIAGO".isupper())
        self.assertFalse("Tiago".isupper())

    def testa_palavras(self):
        nome = "Tiago P Lima"
        self.assertEqual(nome.split(), ["Tiago", "P", "Lima"])

if __name__ == "__main__":
    unittest.main()
