#######################Aula 1 ################################
# Aula 01
#"Print" e utilizado para enviar um conteudo ao usuario.
#print("ola meu nome e Rosana")

# "Input" e utlizado para coletar conteudo do usario.
#input("Qual e o seu nome? ")

# Variaveis sao palavras que armazenam um valor com valor definido
#nome = "Rosana"
#print(nome)
#print("nome")

nome = input("Qual e o seu nome? ")
print ("Seja muito bem vinda", nome)



#####################AULA 2#################################
# Tipos Primitivos - Tipos de dados basicos do Phyton

# String -> str -> frases ou conjuntode caraceres, sempre entre aspas
# Integer -> int -> Numeros inteiros, sem aspas
# Float   -> float -> Numeros decimais, num reais, num com virgula
# Boolean -> bool -> True ou False  -> tem que comecar com a letra maiuscula
# tudo pode ser com aspas duplas ou simples, mas tem que abrir e fechar com a mesma


nome = "Rosana" # String
idade = 27 #integer
# se criasse a variavel idade = "27" seria uma string e nao um num

altura = 1.79 # float
acordado = True #boolean

numero1= "10"
numero2 = "25"
numero3 = numero1 + numero2
print(numero3)

numero4 = 10
numero5 = 25.6
numero6 = numero4 + numero5
print(numero6)
frase1 = "uma gota d'agua "
frase2 = 'ele estava "Acordado" hoje '
print (frase1)
print (frase2)
frase3 = """ Testando frase em cada linha na mesma variavel
essa ja e alinha 2 da var fras3
linha 3 da variavel frase3"""
print (frase3)

# Operadores aritimetricos  + - * /

# Operadores de comparacao
# igauldade -> ==
# desigualdade -> !=
print("compara numero sinal igual ", numero4 == numero5)
print("compara numero sinal diferente ", numero4 != numero5)

#maior que ->  >
#menor que ->  <
#menor ou igual -> <=
#maior ou igual -> >=

#para transformarnumero em string
idade = str(idade) # note que a var idade la em cima e numero. agora ela e string

#voltando a idade pra interger
idade = int(idade)

#convertaendo interger pra float
idade = float(idade)

print(f"""-------------------------
      Confirmacao de cadastro:
      Nome:{nome}
      CPF:{cpf}
      Idade:{idade}
      ------------------------------""")

print("""------------------------
      Confirmacao de cadastro:
      Nome: {}
      CPF:{}
      Idade:{}
      -------------------.format(nome,cpf,idade)""")


# para mudar letras
nome = tiago
print(nome.replace("T","TH"))

#estrutura de decisao
idade = int(input("Qual a sua idade: "))
if idade >= 18:
    print("voce pode entrar.")
    print(voce tem mais de 18)

if idade <=:
print("voce e menor de idade, nao pode entrar")



if idade >= 18:
    print("voce pode entrar")
elif idade >=15:
    print("voce pode entrar, mas nao pode usar o bar")

else:
    print("voce nao pode entrar")

#Estrutura de repeticao
#WHILE e FOR
    
    idade = 15
    while idade <=17:
        print("voce e muito novo pra entrar.")
        idade = int(input("Qual dua idade?:"))
        print("Proximo")
        idade = int(input("Qual dua idade?:"))

    contador = 0
    while contador < 10:
        print("contando 10 vezes")
        contador = contador + 1

    while True:
        resposta = input("voce quer parar o programa S ou N: ".upper())
        if resposta == "S":
            break

    for numero in range(0, 10):
        print(numero)

    for letra in "Tiago P. Lima":
        print(letra)
    
#####################AULA 3#################################
        
#colecoes
# Tuplas, Listas, Dicionarito, Sets

##Tuplas        
#Tuplas sao imutaveis. nao consigo alterar o que tem dentro
#Tuplas possuem index, ou seja o Arroz e a posicao 0, queijo, leite 3 na 1 etc
variavel1 = ("Arroz", "Queijo", "Leite", "Beterraba")
print(variavel1)
        
for item in variavel1:
    print(item)
    
for item in varialvel1:
    print(variavel1[3])

#####Listas
#Listas possuem index, ou seja o Arroz e a posicao 0, queijo, leite 3 na 1 etc
#Listas sao mutaveis    
var = ["Arroz", "Queijo", "Leite", "Beterraba"]
for item in var
    print(item)
for item in varialvel1:
    print(var[3])
#diz qual e a posicao de Leite na lista
print(var.index(Leite))   

var = ["Arroz", "Queijo", "Leite", "Beterraba", "Leite"]
#diz qtas vezes o leite aparece
print(var.count("Leite"))
#diz qtos item no total tem
print(len(var))

#remove o item pelo nome
var.remove("Queijo")
print(var)
#pop remove pelo index
var.pop(3)

#adiciona item no final
var.append("acucar")
#adiciona item no index escolhido
var.insert(0,"batata")
# vejo que item este em qual posicao
for item in enumerate(var):
    print(item)


var = {"idade": [12, 15, 20, 35], "cor": "preto", "animal": "gato"}

for x in var["idade"]:
    if x .= 18
        print(x)


#####Dicionario
#dicionario nao possui index e tem desempenho melhor que listas
#dicionarios, no tem chave repetidas
    
# essa var1 tem 3objetos que cote Chave: Valor
var1 = {"Idade": 13, "cor": "vermelho", "animal": "gato"}
print(var1)
print(var1["animal"])
#Adiciona no final
var1["nome"] = "Tiago"

# pop deleta o item idade do dicionario
var1.pop("Idade")
#del deleta o item cor do dicionario
del(var1["cor"])


for objeto in var1:
    print(objeto)

for objeto in var1.item():
    print(objeto)

for objeto in var1.keys():
    print(objeto)

for objeto in var1.values():
    print(objeto)

#Sets
#para criar um set a gente avisa que e um set e usa () ou nao avisa, mas usa {}
#consgue adicionar ou remover chaves. 
#E possivel ter item repetido,pq nao tem key
var3 = set()
var4 = {"Idade", "cachorro", 12, false, 50}
var = set(var)
var = list(var)

#####################Aula 04####################

import os
idade = 13

def limpa_tela():
    if os.name == "nt":
        os.system("cls")
    else:
        os.system("clear")
limpa_tela()

def boas_vindas():
    nome  = input("Qual e o seu nome: ")
    print(f"seja bem vindo {nome}!")
    print(idade)
boas_vindas()

def soma(x, y):
    resultado = x + y
    print(resultado)

#passando os valores fixos pra dentro da funcao soma na hora da execucao
soma(1,25)
#passando os valores digitados  pra dentro da funcao soma na hora da execucao
resp = int(input("Digite um nomero: "))
soma(resp,10)

def soma1(x, y):
    resultado = x + y
    return resultado
print(f"A soma de 14 e 29 e {soma1(14, 29)}")

def soma2(x, y):
    resultado = x + y
    return resultado
resp1 = int(input("Digite um nomero: "))
resp2 = int(input("Digite um nomero: "))
soma2(resp1, resp2)
print(f"A soma e {soma1(resp1, resp2)}")



def soma(x, y=1):
    resultado = x + y
print(soma(5, 6))
print(soma(5))
print(soma(x=10, y=6)


#colocando o * eu sou obrigada a dizer qual e qual  do x e qual e o y
def soma(*, x, y):
    resultado = x + y
    return resultado
print(soma(x=5, y=6))

#colocando a / eu nao posso forcar qual e o x e y, mas isso vai ser entendido posicionalemtne
def soma(x, y, /):
    resultado = x + y
    return resultado
print(soma(5, 6))

# definiri funcao de multiplicacao e vai uma qtde variavel de parametros. tupla
def mult(*x,y):
    for valor in x:
        print(f"{valor} x {y} = {valor * y}")
    
mult(10, 3, y=1)

#criando um numeor indefinido de parametros. como um dicionrio
def diff(a, **chave):
# nao vai mostrar o a, pq ja tem valo definido.. o resto e que e o dicionario
    print(chave)

diff(a = 10, b=20, c=45, d=120)
diff(a=10, b=20, c=45)
diff(a=10, b=20, c=45, d=20, e=500, f=450)

def diff(a, **chave):
    for valor in chave.values():
        print(f"{a} - {valor}, = {a - valor}")
diff(a = 10, b=20, c=45, d=120)

#funcoes lambida ou funcoes anonimas, note que a lambida ta com cor diferente
# isso e pq ela e uma variavel que contem uma funcao

def som1(x, y):
    return(x+ y)

som2 = lambda x, y: x + y

print(som1(10, 15))
print(som2(10, 15))

#modulos
#existem 3 maneiras de ter acesso a novos modulos no phyton

#existem modulos NATIVOS e carregados como o print, input etc
#existem s que sao NATIVOS, mas nao sao importados por padrao, como o:
# os, o  math, o random etc
import os, math, random
# existem os modulos NAO NATIVOS que podem ser baixados pelo gerenciador
# de pacote PIP como o flask, pymong etc
#PIP baixa pacote ofical do python
import flask
#Existe como criar um modulo e disponibilizar para que outros possam usar
# pra baixar, vc vai la no git da pessoa e 
import modulo_baixado
modulo_baixado.imprime_mensagem()
#tambem posso usar dessa forma
from modulo_baixado import imprime_mensagem


#----------------------------Aula 05------------------------------

#Trabalhar com arquivos
#w - wite
# - read
# a = append

import csv
arquivo = open("teste.txt", "a")
conteudo = "primeira linha do arquivo\n"
arquivo.write(conteudo)
arquivo.close()

arquivo = open("teste.txt", "r")
conteudo = arquivo.read()
print(conteudo)
arquivo.close()

#with open("teste.txt", "a") as aquivo:
#    conteudo = "primeira linha do arquivo with\n"
#    arquivo.write(conteudo)

import csv

#com csv
with open("arquivo.csv", "r") as planilha:
    conteudo =csv.reader(planilha, delimiter=":")
#generator
    print(next(conteudo))
    for linha in conteudo:
        print(linha)
        print(linha[1])

#modulo time








