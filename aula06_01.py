# 1) Crie uma classe que represente um ônibus. O ônibus deverá conter os seguintes atributos:

# capacidade total
# capacidade atual
# movimento

# Os comportamentos esperados para um Ônibus são:
# Embarcar
# Desembarcar
# Acelerar
# Frear

# Lembre-se que a capacidade total do ônibus é de 45 pessoas - não será possível admitir super-
# lotação. Além disso, quando o ônibus ficar vazio, não será permitido efetuar o desembarque
# de pessoas. Além disso, pessoas não podem embarcar ou desembarcar com o onibus em movimento.
class Busao:

    def __init__(self):
        self.movimento = False
        self.captotal = 45
        self.capatual = 0

    def acelerar(self, move):
        self.movimento == True
        print("Onibus acelerando. Ninguem embarca!")
    
    def frear(self, move):
            self.movimento == False
            print("Onibus parado")

    def embarque(self, maisum):
        if self.movimento == False:
            if self.capatual + maisum < self.captotal:
                self.captotal += maisum
                print(f"Embarque de", maisum, "passageiros")
            else:
                print("Lotacao maxima atingida, ninguem mais pode embarcar!")
        else:
            print("Onibus em movimento. Ninguem pode embarcar")
    
    def desembarque(self, menosum):
        if self.movimento == False:
            if self.capatual - menosum > self.captotal:
                self.captotal -= menosum
                print(f"Desembarque de", menosum, "passageiros")
            else:
                print("Nao ha mas nenhum passageiro para desembarcar!")
        else:
            print("Onibus em movimento. Ninguem pode desembarcar")
    
        


rua1 = Busao()  #busao linha 1

rua1.acelerar(True)
rua1.embarque(30)
rua1.desembarque(4)
